# README #

Setup commercial paper network

### Add PATH environment variable ###

* Add folder commercial-paper-chaincode/bin into your PATH environment variable

### Bring the network up ###

* cd commercial-paper
* ./network-starter.sh

### Deploy chaincode to MagnetoCorp ###

* source magnetocorp.sh
* peer lifecycle chaincode package cp.tar.gz --lang node --path ./contract --label cp_0
* peer lifecycle chaincode install cp.tar.gz
* export PACKAGE_ID={package indentity got from the above command}
* peer lifecycle chaincode approveformyorg --orderer localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name papercontract -v 0 --package-id $PACKAGE_ID --sequence 1 --tls --cafile $ORDERER_CA

### Deploy chaincode to DigiBank ###

* source digibank.sh
* peer lifecycle chaincode package cp.tar.gz --lang node --path ./contract --label cp_0
* peer lifecycle chaincode install cp.tar.gz
* export PACKAGE_ID={package indentity got from the above command}
* peer lifecycle chaincode approveformyorg --orderer localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name papercontract -v 0 --package-id $PACKAGE_ID --sequence 1 --tls --cafile $ORDERER_CA

### Commit chaincode ###

* peer lifecycle chaincode commit -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --peerAddresses localhost:7051 --tlsRootCertFiles ${PEER0_ORG1_CA} --peerAddresses localhost:9051 --tlsRootCertFiles ${PEER0_ORG2_CA} --channelID mychannel --name papercontract -v 0 --sequence 1 --tls --cafile $ORDERER_CA --waitForEvent

### Check again the network ###

* docker ps

### Add wallets ###

* cd commercial-paper/organization/digibank/application
* node enrollUser.js
* node addToWallet.js
* cd commercial-paper/organization/magnetocorp/application
* node enrollUser.js
* node addToWallet.js

### Copy gateways and wallets to project api ###

* cp commercial-paper/organization/digibank/gateway/ ../commercial-paper-api/config/connection
* cp commercial-paper/organization/magnetocorp/gateway/ ../commercial-paper-api/config/connection
* cp commercial-paper/organization/digibank/identity/user/balaji/wallet/ ../commercial-paper-api/config/wallet
* cp commercial-paper/organization/digibank/identity/user/isabella/wallet/ ../commercial-paper-api/config/wallet

### Run the api ###

* cd commercial-paper-api
* node app.js
